import fire as fire
import urllib3
from loguru import logger

from ns_conf import process_namespace
from ns_conf.config import config_factory, ValidationError
from ns_conf.kubernetes import KubernetesClient
import ns_conf.kubernetes_dry_run


class ClusterConfCli(object):

    def apply(self, input_file: str = None, dry_run: bool = False, disable_ssl_warning: bool = False) -> None:
        """
        Apply configuration to OpenShift
        :param input_file: Where to read input data from, default is stdin
        :param dry_run: Run but do not change anything
        :param disable_ssl_warning: Do not print warning on unknown cert
        :return:
        """
        if disable_ssl_warning:
            urllib3.disable_warnings()
        if dry_run:
            kube_cfg = KubernetesClient(k8s_module=ns_conf.kubernetes_dry_run)
        else:
            kube_cfg = KubernetesClient()

        config = config_factory(input_file, kube_cfg)
        for ns in config['namespaces']:
            logger.debug(f"Processing namespace {ns}.")
            process_namespace.sync_namespace(ns, kube_cfg)
        if not dry_run:
            logger.info("Configuration applied!")

    def validate(self, input_file: str = None, disable_ssl_warning: bool = False) -> None:
        """
        Validate configuration file
        :param input_file: Where to read input data from, default is stdin
        :param disable_ssl_warning: Do not print warning on unknown cert
        :return:
        """
        if disable_ssl_warning:
            urllib3.disable_warnings()
        kube_cfg = KubernetesClient()

        try:
            config_factory(input_file, kube_cfg)
            print("Validation passed!")
        except ValidationError as e:
            print("Validation failed! ")
            print(e)


def main():
    fire.Fire(ClusterConfCli)


if __name__ == '__main__':
    main()
