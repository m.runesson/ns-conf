from loguru import logger

import kubernetes
from ns_conf.kubernetes_dry_run.client.exceptions import ApiException


class CustomObjectsApi:

    def list_cluster_custom_object(self, group, version, plural):
        logger.info(f"List objects {plural}.{group}/{version}")
        try:
            return kubernetes.client.CustomObjectsApi().list_cluster_custom_object(group, version, plural)
        except kubernetes.client.exceptions.ApiException as e:
            logger.info(f"Failed list objects {plural}.{group}/{version}: {e.reason}")
            raise ApiException(e)


class CoreV1Api:

    def list_namespaces(self):
        logger.info(f"List namespaces")
        try:
            return kubernetes.client.CoreV1Api().list_namespace()
        except kubernetes.client.exceptions.ApiException as e:
            logger.info(f"Failed namespaces: {e.reason}")
            raise ApiException(e)

    def read_namespace(self, namespace):
        logger.info(f"Reading namespace {namespace}")
        try:
            return kubernetes.client.CoreV1Api().read_namespace(namespace)
        except kubernetes.client.exceptions.ApiException as e:
            logger.info(f"Failed reading namespace {namespace}: {e.reason}")
            raise ApiException(e)

    def create_namespace(self, body):
        logger.info(f"Create namespace with {body}")

    def patch_namespace(self, namespace, patch):
        logger.info(f"Patch namespace {namespace} with {patch}")


class RbacAuthorizationV1Api:

    def read_namespaced_role_binding(self, name, namespace):
        logger.info(f"Read role binding {name} in namespace {namespace}")
        try:
            return kubernetes.client.RbacAuthorizationV1Api().read_namespaced_role_binding(name, namespace)
        except kubernetes.client.exceptions.ApiException as e:
            logger.info(f"Failed read role binding {name} in namespace {namespace}: {e.reason}")
            raise ApiException(e)

    def create_namespaced_role_binding(self, namespace, body):
        logger.info(f"Create role binding in namespace {namespace} with {body}")

    def patch_namespaced_role_binding(self, name, namespace, patch):
        logger.info(f"Patch role binding {name} in namespace {namespace} with {patch}")

