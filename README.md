# Cluster-conf

Cluster-conf is a tool to help to manage namespaces in OpenShift Container plattform. It does not work
with native Kubernetes clusters since it is dependent on the `user.openshift.io`.

Cluster conf takes an input file looking like:
```yaml
namespaces:
- name: demo
  # Example namespace to show structure.
  team: revival
  # All in team revival will get admin permissions
  labels:
  # We can set extra labels if needed.
    extra-label: a value
    argocd.argoproj.io/managed-by: openshift-gitops
    # This label gives ArgoCD access to the namespace.
  annotations:
  # We can set extra annotations if needed.
    extra-annotation: another value
- name: runmm-personal-project
  user: runmm
  # If user is used, no team access will be set.
  # Cannot be used together with team label.
```

The script ensure all namespaces exists and has a label `jobtechdev.se/team` assigned with the team name as value.
This team group does get the admin permissions to the namespace (bound to cluster role `admin`). If user is 
used instead of team, admin permissions are set for that user on the namespace. If extra annotations or 
labels are provided, these will be assigned to the namespace.

The script never removes labels or annotations on namespaces, nor does it remove namespaces. This principle
was selected to avoid conflicting with other configuration tools and to avoid destroying work.

## Run local as developer

Program uses poetry. Easiest way to run the script after you hae set up the
environment is to do `poetry run ns-conf`

## Note about removing projects
To remove projects, you currently need to do the following:

- Remove the Argocd project, in the argocd-infra. Please note that not
  all namespaces has argocd projects. For instance -develop
  namespaces.

- Remove the corresponding line in the correct kustomize file, this
  for production:
  
  ns-conf-infra/kustomize/overlays/prod/namespaces.yaml

- Wait 10 min so the MR has been merged and scheduled.

- Remove the namespace manually with oc
