import unittest
from unittest.mock import Mock, MagicMock

from ns_conf.process_namespace import sync_namespace

NAMESPACE_NAME = 'my-namespace'
TEAM = 'my-team'
USER = 'my-user'


class SyncNamespaceTest(unittest.TestCase):

    def setUp(self) -> None:
        self.k8s_client = Mock()
        self.k8s_client.get_namespace = Mock()
        self.k8s_client.create_namespace = Mock()
        self.k8s_client.set_access_in_namespace = Mock()

    def test_sync_namespace_for_new_namespace_with_team(self):
        self.k8s_client.get_namespace.return_value = {}
        config = {
            'name': NAMESPACE_NAME,
            'team': TEAM
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.create_namespace.assert_called_with(NAMESPACE_NAME, {'jobtechdev.se/team': TEAM}, {})
        self.k8s_client.set_access_in_namespace.assert_called_with(NAMESPACE_NAME, 'admin', 'admin', [], [TEAM])

    def test_sync_namespace_for_new_namespace_with_user(self):
        self.k8s_client.get_namespace.return_value = {}
        config = {
            'name': NAMESPACE_NAME,
            'user': USER
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.create_namespace.assert_called_with(NAMESPACE_NAME,
                                                       {'jobtechdev.se/user': USER},
                                                       {'openshift.io/requester': USER})
        self.k8s_client.set_access_in_namespace.assert_called_with(NAMESPACE_NAME, 'admin', 'admin', [USER], [])

    def test_sync_namespace_for_existing_namespace_with_team(self):
        self.k8s_client.get_namespace.return_value = {'metadata': {
                                                        'name': NAMESPACE_NAME}}
        config = {
            'name': NAMESPACE_NAME,
            'team': TEAM
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.add_metadata_to_namespace.assert_called_with(NAMESPACE_NAME, {'jobtechdev.se/team': TEAM}, {})
        self.k8s_client.set_access_in_namespace.assert_called_with(NAMESPACE_NAME, 'admin', 'admin', [], [TEAM])

    def test_sync_namespace_for_existing_namespace_with_team_and_correct_metadata(self):
        self.k8s_client.get_namespace.return_value = {'metadata': {
                                                        'name': NAMESPACE_NAME,
                                                        'labels': {'jobtechdev.se/team': TEAM}}}
        config = {
            'name': NAMESPACE_NAME,
            'team': TEAM
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.add_metadata_to_namespace.assert_not_called()
        self.k8s_client.set_access_in_namespace.assert_called_with(NAMESPACE_NAME, 'admin', 'admin', [], [TEAM])

    def test_sync_namespace_for_existing_namespace_with_user(self):
        self.k8s_client.get_namespace.return_value = {'metadata': {
                                                        'name': NAMESPACE_NAME}}
        config = {
            'name': NAMESPACE_NAME,
            'user': USER
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.add_metadata_to_namespace.assert_called_with(NAMESPACE_NAME,
                                                                     {'jobtechdev.se/user': USER},
                                                                     {'openshift.io/requester': USER})
        self.k8s_client.set_access_in_namespace.assert_called_with(NAMESPACE_NAME, 'admin', 'admin', [USER], [])

    def test_sync_namespace_for_existing_namespace_with_user_and_correct_metadata(self):
        self.k8s_client.get_namespace.return_value = {'metadata': {
                                                        'name': NAMESPACE_NAME,
                                                        'labels': {'jobtechdev.se/user': USER},
                                                        'annotations': {'openshift.io/requester': USER}}}
        config = {
            'name': NAMESPACE_NAME,
            'user': USER
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.add_metadata_to_namespace.assert_not_called()
        self.k8s_client.set_access_in_namespace.assert_called_with(NAMESPACE_NAME, 'admin', 'admin', [USER], [])

    def test_sync_namespace_for_namespace_with_labels(self):
        self.k8s_client.get_namespace.return_value = None
        config = {
            'name': NAMESPACE_NAME,
            'team': TEAM,
            'labels': {
                'my-label': 'my-label-value'
            }
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.create_namespace.assert_called_with(NAMESPACE_NAME,
                                                            {'jobtechdev.se/team': TEAM, 'my-label': 'my-label-value'},
                                                            {})

    def test_sync_namespace_for_existing_namespace_with_annotation(self):
        self.k8s_client.get_namespace.return_value = {'metadata': {}}
        config = {
            'name': NAMESPACE_NAME,
            'user': USER,
            'annotations': {
                'annotation-1': 'annotation-value'
            }
        }

        sync_namespace(config, self.k8s_client)

        self.k8s_client.get_namespace.assert_called_with(NAMESPACE_NAME)
        self.k8s_client.add_metadata_to_namespace.assert_called_with(
            NAMESPACE_NAME,
            {'jobtechdev.se/user': USER},
            {'annotation-1': 'annotation-value', 'openshift.io/requester': USER})


if __name__ == '__main__':
    unittest.main()
