import unittest

from ns_conf import config


class ConfigValidationTest(unittest.TestCase):

    def test_validate_namespace_with_team_is_accepted(self):
        self.assertIsNone(config._validate_namespace({'name': 'foo', 'team': 'bar'}))

    def test_validate_namespace_with_user_is_accepted(self):
        self.assertIsNone(config._validate_namespace({'name': 'foo', 'user': 'bar'}))

    def test_validate_namespace_with_illegal_name_fails(self):
        self.assertRaisesRegex(config.ValidationError,
                               'Not valid namespace.*',
                               config._validate_namespace,
                               {'name': '/#)¤%(//#/', 'user': 'bar'})

    def test_validate_namespace_with_user_and_team_fails(self):
        self.assertRaisesRegex(config.ValidationError,
                               '.* team or user.*',
                               config._validate_namespace,
                                {'name': 'foo', 'user': 'bar', 'team': 'gazonk'})

    def test_validate_namespace_without_user_or_team_fails(self):
        self.assertRaisesRegex(config.ValidationError,
                               '.* team or user.*',
                               config._validate_namespace,
                               {'name': 'foo'})

    def test_validate_namespace_without_name_fails(self):
        self.assertRaisesRegex(config.ValidationError,
                               'No name .*',
                               config._validate_namespace,
                               {'team': 'gazonk'})

    def test_validate_namespace_with_labels(self):
        self.assertIsNone(
            config._validate_namespace(
                {'name': 'foo', 'user': 'bar', 'labels': {'label1': 'foo', 'label2': 'bar'}}))

    def test_validate_namespace_with_annotations(self):
        self.assertIsNone(
            config._validate_namespace(
                {'name': 'foo', 'user': 'bar', 'annotations': {'annotation1': 'foo', 'annotation2': 'bar'}}))

    def test_key_value_pair_is_correct(self):
        self.assertTrue(config._are_valid_key_value_pairs({'label-key': 'label-value'}))

    def test_key_value_pair_is_correct_with_label_prefix(self):
        self.assertTrue(config._are_valid_key_value_pairs({'jobtechdev.se/label-key': 'label-value'}))

    def test_key_value_pair_without_faulty_key_return_false(self):
        self.assertFalse(config._are_valid_key_value_pairs({'-value': 'bar'}))

    def test_key_value_pair_without_value_is_true(self):
        self.assertTrue(config._are_valid_key_value_pairs({'value': None}))

    def test_validate_values_from_cluster_with_matching_user(self):
        all_users = ['foo', 'bar']
        all_groups = ['gazonk']
        ns = {'user': 'bar'}
        self.assertIsNone(config._validate_namespace_with_users_and_groups(ns, all_users, all_groups))

    def test_validate_values_from_cluster_with_matching_group(self):
        all_users = ['gazonk']
        all_groups = ['foo', 'bar']
        ns = {'team': 'bar'}
        self.assertIsNone(config._validate_namespace_with_users_and_groups(ns, all_users, all_groups))

    def test_validate_values_from_cluster_with_non_matching_user(self):
        all_users = ['foo', 'bar']
        all_groups = ['gazonk']
        ns = {'user': 'gazonk'}
        self.assertRaisesRegex(config.ValidationError,
                               'User .*',
                               config._validate_namespace_with_users_and_groups,
                               ns, all_users, all_groups)

    def test_validate_values_from_cluster_non_with_matching_group(self):
        all_users = ['gazonk']
        all_groups = ['foo', 'bar']
        ns = {'team': 'gazonk'}
        self.assertRaisesRegex(config.ValidationError,
                               'Group.*',
                               config._validate_namespace_with_users_and_groups,
                               ns, all_users, all_groups)


if __name__ == '__main__':
    unittest.main()
