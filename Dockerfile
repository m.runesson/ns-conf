FROM docker-images.jobtechdev.se/mirror/python:3.10.5-slim-bullseye

COPY . /app
WORKDIR /app

RUN python -m pip install --upgrade build
RUN python -m build --outdir /app/out

FROM  docker-images.jobtechdev.se/mirror/python:3.10.5-slim-bullseye

ENV APP_VER=0.1.0

WORKDIR /app
COPY --from=0 /app/out/ns_conf-${APP_VER}-py3-none-any.whl ./
RUN python -m pip install ns_conf-${APP_VER}-py3-none-any.whl
